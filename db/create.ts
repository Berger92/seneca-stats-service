import * as dotenv from 'dotenv';
dotenv.config();

import { DataMapper } from "@aws/dynamodb-data-mapper";
import { DynamoDB } from "aws-sdk";

import { UserCourseSession } from "../src/models/daos";

const mapper: DataMapper = new DataMapper({
    client: new DynamoDB({
        region: "eu-west-2",
        apiVersion: "2012-08-10",
        endpoint: process.env.DB_ENDPOINT
    })
});

(async () => {
    await mapper.ensureTableExists(UserCourseSession, {
        writeCapacityUnits: 5,
        readCapacityUnits: 5
    })
    console.log("UserSessionStats table has been created");
})();
