import { DataMapper } from "@aws/dynamodb-data-mapper";
import { DynamoDB } from "aws-sdk";

export default new DataMapper({
    client: new DynamoDB({
        region: "eu-west-2",
        apiVersion: "2012-08-10",
        endpoint: process.env.DB_ENDPOINT
    })
});
