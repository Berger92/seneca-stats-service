import {
    attribute,
    hashKey, rangeKey,
    table
} from '@aws/dynamodb-data-mapper-annotations';

import { Stats as StatsInterface } from "@/models/dtos";

class Stats implements StatsInterface {
    @attribute({
        attributeName: 'AverageScore',
        attributeType: "N"
    })
    averageScore: number;

    @attribute({
        attributeName: 'TimeStudied',
        attributeType: "N"
    })
    timeStudied: number;

    @attribute({
        attributeName: 'TotalModulesStudied',
        attributeType: "N"
    })
    totalModulesStudied: number;
}

@table('UserCourseSessions')
class UserCourseSession {
    @hashKey({
        attributeName: 'UserId',
        attributeType: "S"
    })
    userId: string;

    @rangeKey({
        attributeName: 'Id',
        attributeType: "S"
    })
    id: string;

    @attribute({
        attributeName: 'Stats'
    })
    stats: Stats;

    public static generateId(courseId: string, sessionId: string) {
        return `${courseId}_${sessionId}`;
    }
}

export {
    UserCourseSession,
    Stats
}
