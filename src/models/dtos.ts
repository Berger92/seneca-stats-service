import { IsInt, IsNumber, Min, Max, IsUUID } from "class-validator";

interface Stats {
    totalModulesStudied: number;
    averageScore: number;
    timeStudied: number;
}

class AddStatsRequest implements Stats {
    @IsInt()
    @Min(0)
    totalModulesStudied: number;

    @IsNumber()
    @Min(0)
    @Max(100)
    averageScore: number;

    @IsInt()
    @Min(0)
    @Max(3600000)
    timeStudied: number;

    @IsUUID('4')
    sessionId: string;
}

export {
    Stats,
    AddStatsRequest
}
