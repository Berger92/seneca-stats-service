import { IsUUID } from "class-validator";

class CourseStatsParams {
    @IsUUID('4')
    courseId: string;
}

class CourseSessionStatsParams extends CourseStatsParams {
    @IsUUID('4')
    sessionId: string;
}

export {
    CourseStatsParams,
    CourseSessionStatsParams
}
