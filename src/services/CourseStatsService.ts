import { DataMapper } from "@aws/dynamodb-data-mapper";

import { UserCourseSession, Stats as StatsDataModel } from "@/models/daos";
import { Stats } from "@/models/dtos";

export default class CourseStatsService {
    constructor(private readonly mapper: DataMapper) {}

    async getSessionStats(userId: string, courseId: string, sessionId: string): Promise<Stats|null> {
        const id = UserCourseSession.generateId(courseId, sessionId);
        const userSession = new UserCourseSession();
        userSession.userId = userId;
        userSession.id = id;

        try {
            const result = await this.mapper.get(userSession);

            return result.stats;
        } catch (e) {
            if (e.name === 'ItemNotFoundException') {
                return null;
            } else {
                throw e;
            }
        }
    }

    async addSessionStats(userId: string, courseId: string, stats: Stats & {sessionId: string}): Promise<void> {
        const session = new UserCourseSession();
        session.userId = userId;
        session.id = UserCourseSession.generateId(courseId, stats.sessionId);

        session.stats = new StatsDataModel();

        session.stats.averageScore = stats.averageScore;
        session.stats.timeStudied = stats.timeStudied;
        session.stats.totalModulesStudied = stats.totalModulesStudied;

        await this.mapper.put(session);
    }

    async getAggregatedStats(userId: string, courseId: string): Promise<Stats|null> {
        const stats: Stats[] = [];

        for await (const session of this.mapper.query(
            UserCourseSession, {
                type: "And",
                conditions: [
                    {
                        subject: "userId",
                        type: "Equals",
                        object: userId
                    },
                    {
                        type: "Function",
                        subject: "id",
                        name: "begins_with",
                        expected: courseId
                    }
                ]
            }
        )) {
            stats.push(session.stats);
        }

        return CourseStatsService.calculateAverageStats(stats);
    }

    protected static calculateAverageStats(stats: Stats[]): Stats {
        let totalModulesStudied = 0;
        let averageScore = 0;
        let timeStudied = 0;

        for (let item of stats) {
            totalModulesStudied += item.totalModulesStudied;
            averageScore += item.averageScore;
            timeStudied += item.timeStudied;
        }

        if (stats.length > 0) {
            totalModulesStudied /= stats.length;
            averageScore /= stats.length;
            timeStudied /= stats.length;
        }

        return {
            totalModulesStudied,
            averageScore,
            timeStudied
        }
    }
}
