import "reflect-metadata";
import 'source-map-support/register';

import * as serverless from "aws-serverless-express";
import { APIGatewayEvent, Context } from "aws-lambda";

import createApp from "./app";

const server = serverless.createServer(createApp("/stats"));

exports.handler = (event: APIGatewayEvent, context: Context) => { serverless.proxy(server, event, context) };
