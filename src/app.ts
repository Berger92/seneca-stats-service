import { Action, createExpressServer, useContainer } from "routing-controllers";
import { isUUID } from "class-validator";

import Container from "./container";

export default function createApp(routePrefix?: string) {
    useContainer(Container);

    // It seems that all headers are converted to lowercase
    const userIdHeader = "x-user-id";

    return createExpressServer({
        controllers: [__dirname + "/controllers/*"],
        routePrefix: routePrefix,
        authorizationChecker: (action: Action) => {
            return isUUID(action.request.headers[userIdHeader], '4');
        },
        currentUserChecker: (action: Action) => {
            const userId = action.request.headers[userIdHeader];

            return {
                userId
            };
        }
    });
}
