import { Container } from "typedi";

import DataMapper from "@/utils/DataMapper";
import CourseStatsService from "@/services/CourseStatsService";

// injecting course stats service
Container.set("COURSE_STATS_SERVICE", new CourseStatsService(DataMapper));

export default Container;
