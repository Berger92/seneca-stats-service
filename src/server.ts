import * as dotenv from 'dotenv';
dotenv.config();

import "reflect-metadata";

import createApp from "./app";

const PORT = process.env.PORT ?? 3000;

(async () => {
    const server = createApp();
    await server.listen(PORT);
    console.log(`server is listening on port ${PORT}`);
})();
