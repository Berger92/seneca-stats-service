import { Inject } from "typedi";
import { Get, Authorized, JsonController, Params, CurrentUser } from "routing-controllers";

import { CourseStatsParams } from "@/models/params";
import { Stats } from "@/models/dtos";
import User from "@/models/User";
import CourseStatsService from "@/services/CourseStatsService";

@JsonController()
export class CourseController {
    private service: CourseStatsService;

    constructor(@Inject("COURSE_STATS_SERVICE") service: CourseStatsService) {
        this.service = service;
    }

    @Get("/courses/:courseId")
    @Authorized()
    public async getCourseStats(
        @Params() { courseId }: CourseStatsParams,
        @CurrentUser() user: User
    ): Promise<Stats> {
        return this.service.getAggregatedStats(user.userId, courseId);
    }
}
