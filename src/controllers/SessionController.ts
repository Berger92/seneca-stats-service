import { Get, Post, Authorized, JsonController, Params, Body, HttpCode, CurrentUser } from "routing-controllers";
import { Inject } from "typedi";

import { CourseSessionStatsParams, CourseStatsParams } from "@/models/params";
import { Stats, AddStatsRequest } from "@/models/dtos";
import CourseStatsService from "@/services/CourseStatsService";
import User from "@/models/User";

@JsonController()
export class SessionController {
    private service: CourseStatsService;

    constructor(@Inject("COURSE_STATS_SERVICE") service: CourseStatsService) {
        this.service = service;
    }

    @Get("/courses/:courseId/sessions/:sessionId")
    @Authorized()
    public async getCourseSessionStats(
        @Params() { courseId, sessionId }: CourseSessionStatsParams,
        @CurrentUser() user: User
    ): Promise<Stats & {sessionId: string}> {
        const stats = await this.service.getSessionStats(user.userId, courseId, sessionId);

        if (!stats) return;

        return Object.assign(stats, {
            sessionId: sessionId
        });
    }

    @Post("/courses/:courseId")
    @Authorized()
    @HttpCode(201)
    public async addCourseSessionStats(
        @Params() { courseId }: CourseStatsParams,
        @CurrentUser() user: User,
        @Body() request: AddStatsRequest
    ) {
        await this.service.addSessionStats(user.userId, courseId, request);

        return "Ok";
    }
}
