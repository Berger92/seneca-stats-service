import Container from "@/container";

test('Container has COURSE_STATS_SERVICE', () => {
    expect(Container.has("COURSE_STATS_SERVICE")).toBe(true);
})
