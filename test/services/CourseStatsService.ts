import { DataMapper } from "@aws/dynamodb-data-mapper";

import CourseStatsService from "@/services/CourseStatsService";
import { UserCourseSession } from "@/models/daos";

const userId = "a053bfdd-be1e-46d0-9f49-d2c78b6362d2";
const courseId = "d94c5b60-d6ea-4a67-b245-1b6b6abc0851";
const sessionId = "d6c9f9fa-3015-4a06-b875-f19f9cb31185";

test('getSessionStats should return starts', async () => {
    const model = {
        userId: userId,
        id: UserCourseSession.generateId(courseId, sessionId),
        stats: {
            totalModulesStudied: 120,
            averageScore: 77.2,
            timeStudied: 120000
        }
    };

    const mockFn = jest.fn().mockReturnValueOnce(model);
    const stub = {
        get: mockFn
    } as unknown as DataMapper;

    const service = new CourseStatsService(stub);
    const stats = await service.getSessionStats(userId, courseId, sessionId);

    // it should call the mapper's get method
    expect(mockFn.mock.calls.length).toBe(1);

    // it should return the stats
    expect(stats).toBe(model.stats);
})

test('it should return null if ItemNotFoundException is thrown', async () => {
    const stub = {
        get: jest.fn().mockImplementation(() => {
            const e = new Error('Item Not Found');
            e.name = 'ItemNotFoundException';

            throw e;
        })
    } as unknown as DataMapper;

    const service = new CourseStatsService(stub);
    const stats = await service.getSessionStats(userId, courseId, sessionId);

    expect(stats).toBeNull();
})
