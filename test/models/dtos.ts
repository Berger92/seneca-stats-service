import { validate } from "class-validator";

import { AddStatsRequest } from "@/models/dtos";

test('AddStatsRequest should be valid', async () => {
    const addStatsRequest = new AddStatsRequest();

    let errors = await validate(addStatsRequest);
    expect(errors.length).toBe(4);

    addStatsRequest.averageScore = 101;
    addStatsRequest.sessionId = "asd";
    addStatsRequest.timeStudied = 13600001;
    addStatsRequest.totalModulesStudied = -2;

    errors = await validate(addStatsRequest);
    expect(errors.length).toBe(4);

    addStatsRequest.averageScore = 77.12;
    addStatsRequest.sessionId = "8c548ace-3d13-453e-b5ec-ceb0ff1e84ef";
    addStatsRequest.timeStudied = 265000;
    addStatsRequest.totalModulesStudied = 1230000;

    errors = await validate(addStatsRequest);
    expect(errors.length).toBe(0);
})
