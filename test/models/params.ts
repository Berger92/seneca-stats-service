import { validate } from "class-validator";

import { CourseSessionStatsParams, CourseStatsParams } from "@/models/params";

test('courseId must be an uuid', async () => {
    const courseStatsParams = new CourseStatsParams();
    courseStatsParams.courseId = "foo";

    let errors = await validate(courseStatsParams);
    expect(errors.length).toBe(1);

    courseStatsParams.courseId = "54a2b8d0-8d7a-429d-808e-d3c4ed4ea62e";
    errors = await validate(courseStatsParams);
    expect(errors.length).toBe(0);
})

test('sessionId must be an uuid', async () => {
    const courseSessionStatsParams = new CourseSessionStatsParams();
    courseSessionStatsParams.courseId = "f3f2c4b9-435b-4ec4-a486-fdf4c797e810";
    courseSessionStatsParams.sessionId = "bar";

    let errors = await validate(courseSessionStatsParams);
    expect(errors.length).toBe(1);

    courseSessionStatsParams.sessionId = "9754c0a2-bfb4-499b-b246-95818d8f104d";
    errors = await validate(courseSessionStatsParams);
    expect(errors.length).toBe(0);
})
